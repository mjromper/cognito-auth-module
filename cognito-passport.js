const passport = require('passport')
const CognitoOAuth2Strategy = require('passport-cognito-oauth2');
const config = require('./config.json');
const qsTicket = require('./qs-ticket');

function _verify(accessToken, refreshToken, profile, done) {
    console.log("Cognito AccessToken", accessToken);
    //console.log("refreshToken",refreshToken);
    console.log("Cognito Authenticated Profile", profile);
    /*axios( {
        method: 'GET',
        url: "https://seatdemo.auth.eu-west-1.amazoncognito.com/oauth2/userInfo",
        headers: {
            'Authorization': "Bearer "+accessToken, 
            'Content-Type': 'application/json' 
        },
        responseType: 'json'
    } ).then(function(res){
        console.log("USER INFO", res.data);
    }).catch(function(err){
        console.log("USER INFO ERR", err);
    })*/
  //User.findOrCreate(profile, (err, user) => {
    done(null, profile);
  //});
}

passport.use( new CognitoOAuth2Strategy(config.cognito, _verify) );  
passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((obj, done) => done(null, obj));

module.exports.init = function(app){
    
    app.use(passport.initialize());

    app.get('/login', function(req, res){
        
        // Save query params in global
        global.qlikAuthSession = req.query;  

        // Redirect to Cognito login page
        const cognitoLoginURL = `${config.cognito.clientDomain}/login?client_id=${config.cognito.clientID}&response_type=code&scope=aws.cognito.signin.user.admin+email+openid+profile&redirect_uri=${config.cognito.callbackURL}`;
        res.redirect(cognitoLoginURL);
    });
    
    app.get('/auth/cognito/callback', passport.authenticate('cognito-oauth2'),  
        function(req,res) { 
            
            // Get authenticated user passed from Passport Oauth
            var user = req.user;

            // Get saved original QS targetId
            var targetId;
            if (global.qlikAuthSession && global.qlikAuthSession.targetId) {
                targetId = global.qlikAuthSession.targetId;
            }

            // Request QS Ticket
            qsTicket.requestTicket(user.name, null, targetId).then(function(result) {
                var ticketResponse = result.data;
                console.log("QS Ticket", ticketResponse);

                // Redirect to QS with the QS ticket
                var redirectUrl = config.qs.hostname + "/" + config.qs.virtualproxy + "/hub?qlikTicket=" + ticketResponse.Ticket;   
                if ( ticketResponse.TargetUri ) {
                    if (ticketResponse.TargetUri.indexOf("?") !== -1){
                        redirectUrl = ticketResponse.TargetUri + '&qlikTicket=' + ticketResponse.Ticket;
                    } else {
                        redirectUrl = ticketResponse.TargetUri + '?qlikTicket=' + ticketResponse.Ticket;                
                    }
                }
                res.writeHead(302, {'Location': redirectUrl});
                res.end();

            }).catch(function(err){
                res.send("nok")  
            });
        }
    );

    app.get('/logout', function(req, res){
        req.logout();
        res.redirect('/login');
    });

};