const axios = require('axios');
const https = require('https');
const fs = require('fs');
const path = require('path');

const config = require("./config.json");
const QRS_URL = `${config.qs.hostname}:4243/qps/${config.qs.virtualproxy}/ticket`;

const HTTPSAGENT = new https.Agent({
    cert: fs.readFileSync(path.resolve(__dirname, 'certs', 'qs', 'client.pem')),
    key: fs.readFileSync(path.resolve(__dirname, 'certs', 'qs', 'client_key.pem')),
    ca: fs.readFileSync(path.resolve(__dirname, 'certs', 'qs', 'root.pem'))
});

function _ticketBody(userId, attributes, targetId){
    return {
        "UserDirectory": config.qs.userDirectory,
        "UserId": userId,
        "Attributes": attributes || [],
        "TargetId": targetId
    };
}

function requestTicket(userId, attributes, targetId) {

    var Xrfkey = "fedcba9876543210";
    var url = QRS_URL + "?Xrfkey=" + Xrfkey;
    var ticketRequest = _ticketBody(userId, attributes, targetId);
    return axios( {
        method: 'POST',
        httpsAgent: HTTPSAGENT,
        url: url,
        headers: {
            'X-Qlik-Xrfkey': Xrfkey, 
            'Content-Type': 'application/json' 
        },
        data: ticketRequest,
        responseType: 'json'
    } );
}

module.exports.requestTicket = requestTicket;
