const express = require("express");
const cognitoPassport = require('./cognito-passport');
const fs = require('fs');
const path = require('path');
const app = express();
const https = require('https');
const bodyParser = require('body-parser');
const config = require('./config.json');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Init passport
cognitoPassport.init(app);

//---- START SERVICE HTTP
app.listen(config.PORT_HTTP, function(){
    console.log(`Auth Module listening in HTTP port: ${config.PORT_HTTP}`);
});

//---- START SERVICE HTTPS
var optionsSsl = {};
if ( config.moduleCerts.use ) {
    //Use provided PFX to serve this module
    optionsSsl = {
        pfx: fs.readFileSync(path.resolve(__dirname, 'certs', 'module', config.moduleCerts.pfxFileName)),
        passphrase: config.moduleCerts.pfxPassphrase
    }
} else {
    //Use Qlik Sense certificates instead
    const serverKey = fs.readFileSync(path.resolve(__dirname, 'certs', 'qs', 'server_key.pem'));
    const serverCert = fs.readFileSync(path.resolve(__dirname, 'certs', 'qs', 'server.pem'));
    optionsSsl = {
        key: serverKey,
        cert: serverCert
    };
}
var server = https.createServer(optionsSsl, app);
server.listen(config.PORT_HTTPS, function() {
    console.log(`Auth Module listening in HTTPS port: ${config.PORT_HTTPS}`);
});

