# AWS Cognito Node Oauth2.0 for QS Ticket Authentication

## Prerequisites

- Node
- NPM 
- Server running this module needs visibilty with Qlik Sense server on port 4243
- Qlik Sense certificates
- OPTIONAL. Your own PFX certificate for this module


## Pull the repository and resolve dependencies

```
git clone https://gitlab.com/mjromper/cognito-auth-module.git
cd cognito-auth-module
npm install
npm install -g node-windows
npm link node-windows
```

## Module Configuration

- Copy Qlik Sense certificates (root.pem, client.pem, client_key.pem, server.pem and server_key.pem) into __certs/qs__ folder.
- Edit __config.json__ with desired Cognito and Qlik Sense parameters
```json
{
    "PORT_HTTP": 4000,
    "PORT_HTTPS": 4001,
    "cognito": {
        "callbackURL": "https://module_hostname:PORT_HTTPS/auth/cognito/callback",
        "clientDomain": "https://<userpool>.auth.<region>.amazoncognito.com",
        "clientID": "xxxxxxxx",
        "clientSecret": "yyyyyyy",
        "region": "<region>"
    },
    "qs": {
        "hostname": "https://<qs-server>",
        "virtualproxy": "cognito",
        "userDirectory": "COGNITO"
    },
    "moduleCerts": {
        "use": false,
        "pfxFileName": "certificate.pfx",
        "pfxPassphrase": "password"
    }
}
```
- OPTIONAL: if you want to use your own PFX certificate for this module, copy PFX file into __certs/module__ folder and set "moduleCerts.use = true" in __config.json__.

## AWS Cognito callback URL
- In AWS Cognito userpool OAuth2.0 set Callback URL(s) as:  __https://module_hostname:HTTPS_PORT/auth/cognito/callback__


## Qlik Sense Virtual Proxy
- In QMC, Create a virtual proxy to use this module with Ticket authentication.
- Set __https://module_hostname:HTTPS_PORT/login__ as the redirect URI.


## Test Module
- Execute

```
npm start
```

- Access Qlik Sense through the configured virtual proxy: __https://QS_server/VIRTUAL_PROXY/hub__

- When you are done testing that the module runs successfully, kill with 'Ctrl+C'

## Install Module as a Windows Service
- Execute

```
node install-windows-service.js
```
- The module should be running on ports (http and https) as configuerd in __config.json__

