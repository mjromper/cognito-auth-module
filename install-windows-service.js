var Service = require('node-windows').Service;
var path = require('path');

const execPath = path.resolve(__dirname, 'index.js');
console.log("Exec file path:", execPath);

console.log
// Create a new service object
var svc = new Service({
  name:'CognitoNodeTicketAuthService',
  description: 'Cognito Node Ticket Auth Service',
  script: execPath
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
    console.log("Service Installed.");
    svc.start();
});

svc.install();